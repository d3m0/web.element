package org.catools.web.tests;

import org.catools.web.drivers.CDriver;
import org.catools.web.drivers.CDriverSession;
import org.catools.web.drivers.providers.CChromeDriverProvider;
import org.catools.web.drivers.providers.CFireFoxDriverProvider;

public class CDriverTest extends CWebTest {
    protected static String BASE_URL = "https://spring-petclinic-community.herokuapp.com/";

    public CDriverTest() {
        super();
    }

    public CFireFoxDriverProvider switchToFireFox() {
        CFireFoxDriverProvider driverProvider = new CFireFoxDriverProvider();
        switchDriverProvider(driverProvider);
        return driverProvider;
    }

    public CChromeDriverProvider switchToChrome() {
        CChromeDriverProvider driverProvider = new CChromeDriverProvider();
        switchDriverProvider(driverProvider);
        return driverProvider;
    }

    @Override
    protected CDriver getDefaultDriver() {
        return new CDriver(new CDriverSession(logger, new CChromeDriverProvider()));
    }
}
