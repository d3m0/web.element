package org.catools.web.tests.visual;

import org.catools.web.enums.CChromeEmulatedDevice;
import org.testng.annotations.BeforeClass;

public class IPadProChromeVisualTest extends VisualTest {

    @BeforeClass(alwaysRun = true)
    @Override
    public void beforeClass() {
        super.beforeClass();
        switchToChrome().setDeviceEmulation(CChromeEmulatedDevice.IPAD_PRO.getDeviceName()).setHeadless(true);
        open(BASE_URL);
    }

}
