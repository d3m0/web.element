package org.catools.web.tests.visual;

import org.catools.common.collections.CList;
import org.catools.common.io.CResource;
import org.catools.common.tests.CTest;
import org.catools.media.utils.CImageUtil;
import org.catools.web.tests.CDriverTest;
import org.openqa.selenium.Dimension;
import org.testng.annotations.Test;

import java.awt.image.BufferedImage;

public class DesktopMultiBrowserVisualTest extends CDriverTest {

    @Test
    public void dashboard() {
        switchSession("CHROME1");
        switchToChrome();
        getDriverSession().setWindowsSize(new Dimension(1024, 1366));
        open(BASE_URL);

        switchSession("CHROME2");
        switchToChrome();
        getDriverSession().setWindowsSize(new Dimension(900, 1024));
        open(BASE_URL);

        switchSession("FIREFOX");
        switchToFireFox();
        getDriverSession().setWindowsSize(new Dimension(1024, 1366));
        open(BASE_URL);

        switchSession("CHROME1");
        getDriver().ScreenShot.verifyEquals(this, new CResource("images/expected/mt_chrome1_petclinic.png", CTest.class), "Screen shot match");

        switchSession("CHROME2");
        getDriver().ScreenShot.verifyEquals(this, new CResource("images/expected/mt_chrome2_petclinic.png", CTest.class), "Screen shot match");

        switchSession("FIREFOX");
        BufferedImage bf1 = CImageUtil.readImageOrNull(new CResource(getName() + "Invalid", CTest.class));
        String resourceFullName = "mt_firefox_petclinic.png";
        BufferedImage bf2 = CImageUtil.readImageOrNull(new CResource("images/expected/" + resourceFullName, CTest.class));
        getDriver().ScreenShot.verifyEqualsAny(this, (Iterable) new CList(bf1, bf2), resourceFullName, 10, "Screen shot match");

        quitAll();
    }

}
