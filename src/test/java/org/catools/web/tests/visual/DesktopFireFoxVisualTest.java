package org.catools.web.tests.visual;

import org.openqa.selenium.Dimension;
import org.testng.annotations.BeforeClass;

public class DesktopFireFoxVisualTest extends VisualTest {

    @BeforeClass(alwaysRun = true)
    @Override
    public void beforeClass() {
        super.beforeClass();
        switchToFireFox();
        getDriverSession().setWindowsSize(new Dimension(1024, 1366));
        open(BASE_URL);
    }
}
